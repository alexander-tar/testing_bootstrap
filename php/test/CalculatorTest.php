<?php

namespace Test;
require __DIR__ . "/../src/Calculator.php";


use PHPUnit\Framework\TestCase;
use SUT\Calculator;

class CalculatorTest extends TestCase
{

    /**
     * @test
     */
    public function addProducesOne()
    {
        $this->assertEquals(1, (new Calculator())->add());
    }
}